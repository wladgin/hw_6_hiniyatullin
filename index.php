<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Регистрация</title>
    <style>
        form{
            margin: 15px;
        }
        #name{
            width: 300px;
        }
        #comments{
            width: 300px;
        }

    </style>
</head>
<body>

<h1 align="center">Регистрация нового пользователя</h1>

<form action="my.php" method="post">
    <label for="name">Как вас зовут:</label>
    <p>
        <input type="text" name="surname" id="name" required placeholder="Ваша фамилия"><br>
        <input type="text" name="name" id="name" required placeholder="Ваше имя"><br>
        <input type="text" name="patronymic" id="name" required placeholder="Ваше отчество"><br>
    </p>
    <label for="type_user">Ваш статус:</label><br>
    <p><select name='type_user'>
        <option value="student">Студент</option>
        <option value="admin">Администратор</option>
        <option value="teacher">Преподаватель</option>
        </select>
    </p>
    <p>
        <label for="age">Ваш возраст:</label><br>
        <input type="text" name="age" id="age" required placeholder="Возраст"><br>
    </p>
    <p>
        <label for="comments">Комментарий</label><br>
        <textarea name="comments" id="comments"></textarea><br>
    </p>
    <button type="submit">Зарегистрироваться</button>
</form>
</body>
</html>