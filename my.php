<?php
/**
 * Created by PhpStorm.
 * User: wladg_000
 * Date: 01.11.2017
 * Time: 1:19
 */

$surname = htmlspecialchars($_REQUEST['surname'], ENT_QUOTES, 'UTF-8');
$name = htmlspecialchars($_REQUEST['name'], ENT_QUOTES, 'UTF-8');
$patronymic = htmlspecialchars($_REQUEST['patronymic'], ENT_QUOTES, 'UTF-8');
$type_user = htmlspecialchars($_REQUEST['type_user'], ENT_QUOTES, 'UTF-8');
$age = htmlspecialchars($_REQUEST['age'], ENT_QUOTES, 'UTF-8');
$comments = htmlspecialchars($_REQUEST['comments'], ENT_QUOTES, 'UTF-8');
?>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Моя страница</title>
</head>
<body>

<?php if($age < 18):?>
    <h2>Извините, но вы слишком молоды..</h2>
    <?=die()?>
<?php endif;?>

<h1>Поздравляем, Вы успешно зарегистрированы и добавлены в список <?=$type_user?>!</h1>

<table border="1">
    <tr>
        <th>Фамилия</th>
        <td><?=$surname ?></td>
    </tr>
    <tr>
        <th>Имя</th>
        <td><?=$name ?></td>
    </tr>
    <tr>
        <th>Отчество</th>
        <td><?=$patronymic ?></td>
    </tr>
    <tr>
        <th>Статус</th>
        <td><?=$type_user ?></td>
    </tr>
    <tr>
        <th>Возраст</th>
        <td><?=$age ?></td>
    </tr>
    <tr>
        <th>Коментарий</th>
        <td><?=$comments ?></td>
    </tr>
</table>

</body>
</html>